package ru.t1.rleonov.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String COMMAND = "project-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return COMMAND;
    }

}
