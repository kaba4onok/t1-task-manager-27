package ru.t1.rleonov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final  String ARGUMENT = "-a";

    @NotNull
    private static final  String NAME = "about";

    @NotNull
    private static final  String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(getPropertyService().getAuthorName());
        System.out.println(getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
