package ru.t1.rleonov.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    public AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
